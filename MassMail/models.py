'''
Created on Apr 8, 2013

@author: CCMT
'''
from django.contrib.auth.models import User
from django.db import models

class Contract(models.Model):
    user = models.ForeignKey(User)
    from_date = models.DateTimeField()
    to_date = models.DateTimeField()
    approved = models.BooleanField(default=False)
    contact_size = models.IntegerField(default=0)
    emails_count = models.IntegerField(default=0)

    def __unicode__(self):
        return 'Contract for %s ' % (self.user)
    