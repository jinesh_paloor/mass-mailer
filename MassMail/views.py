'''
Created on Apr 7, 2013

@author: CCMT
'''
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout as auth_logout

#from MassMail.mongo import get_user, save_user
#from MassMail.validate import validate_signup
from MassMail.forms import SignupForm


@login_required(login_url='/auth/')
def home(request):
    return render_to_response('home.html',context_instance=RequestContext(request))

def signup(request):
    print 'signup  :',request
    # if request method is post validate the data. else just render the signup form.
    if request.method == 'POST':
        form = SignupForm(data=request.POST)
        # if form is valid save user data and start a new session for the user
        if form.is_valid():
            user = form.save()
            return HttpResponseRedirect(reverse('home'))

    else:
        form = SignupForm()
    
    return render_to_response('signup/signup.html', {'form':form}, context_instance=RequestContext(request))

@login_required(login_url='/auth/')
def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/auth/')

@login_required(login_url='/auth/')
def contract(request):
    return render_to_response('home.html',context_instance=RequestContext(request))

