'''
Created on Apr 10, 2013

@author: CCMT
'''
from django.contrib import admin

from MassMail.models import Contract
from Group.models import Group

admin.site.register(Contract)
admin.site.register(Group)