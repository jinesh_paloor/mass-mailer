from django.conf.urls import patterns, include, url

from MassMail.forms import LoginForm


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'MassMail.views.home', name='home'),
    url(r'^signup/$', 'MassMail.views.signup', name='signup'),
    url(r'^logout/$', 'MassMail.views.logout', name='logout'),
    url(r'^contract/$', 'MassMail.views.contract', name='contract'),
    url(r'^group/', include('Group.urls')),
    url(r'^auth/$', 'django.contrib.auth.views.login', { 'template_name' : 'signup/login.html',
          'authentication_form' : LoginForm}, name='auth_login'),
 
    # url(r'^MassMail/', include('MassMail.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
