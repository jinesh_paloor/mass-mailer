'''
Created on Apr 8, 2013

@author: CCMT
'''
import re

# validates that the user information is valid, return True of False 
# and fills in the error codes
def validate_signup(first_name, last_name, email, password, verify, errors={}):
    NAME_RE = re.compile(r"^[a-zA-Z]{3,20}$")
    PASS_RE = re.compile(r"^.{3,20}$")
    EMAIL_RE = re.compile(r"^[\S]+@[\S]+\.[\S]+$")

    errors['firstname_error']  = ""
    errors['lastname_error']  = ""
    errors['password_error'] = ""
    errors['verify_error'] = ""
    errors['email_error'] = ""
    result = True
    

    if not NAME_RE.match(first_name):
        errors['firstname_error']  = "invalid first name. only letters allowed."
        result = False

    if not PASS_RE.match(password):
        errors['password_error'] = "invalid password."
        result = False
    if password != verify:
        errors['verify_error'] = "password must match"
        result = False
    if email != "":
        if not EMAIL_RE.match(email):
            errors['email_error'] = "invalid email address"
            result = False
    return (result, errors)