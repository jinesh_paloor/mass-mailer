'''
Created on Apr 8, 2013

@author: CCMT
'''
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django import forms

class LoginForm(AuthenticationForm):

    username = forms.CharField(label='Username or Email',
                               widget=forms.TextInput(attrs={'rel': 'initFocus'}))

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

class SignupForm(forms.Form):
    username = forms.CharField(label="Username", max_length=30)
    first_name = forms.CharField(label="First Name", max_length=50)
    last_name = forms.CharField(label="Last Name", max_length=50)
    email = forms.EmailField()
    password = forms.CharField(label="Password", widget=forms.PasswordInput)

    def save(self, commit=True):
        """
        For registering the user
        """
        # create a user
        user = User(username=self.cleaned_data['username'],
                    email=self.cleaned_data['email'])
        user.set_password(self.cleaned_data['password'])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.save()

        return user