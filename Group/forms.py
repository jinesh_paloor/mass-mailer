'''
Created on Apr 8, 2013

@author: CCMT
'''
from django import forms

from Group.models import Group, generate_short_code


class GroupForm(forms.ModelForm):

    description = forms.CharField(widget=forms.Textarea(attrs={'class': 'span3'}))

    class Meta:
        model = Group
        fields = ('name', 'description')

    def save(self, user, commit=True):
        # get the group model to add short_code field and user field values manually
        group = super(GroupForm, self).save(commit=False)
        
        # create a unique short code for group
        group.short_code = generate_short_code(user)
        
        # assign user foreign key
        group.user = user
        
        # save the form with rest of existing data.
        if commit:
            group.save()
        
        return group 
    
class ContactsUploadForm(forms.Form):

    gid = forms.IntegerField(widget=forms.HiddenInput)
    contacts_filepath = forms.FileField(
        label='Select a file',
        help_text='Only excel files are supported'
    )
        
    def save(self, commit=True):
        # get the group model to update the contacts file path and update that field
        group = super(ContactsUploadForm, self).save(commit=False)
        group.contacts_filepath = self.cleaned_data['contacts_filepath']
        
        if commit:
            group.save()