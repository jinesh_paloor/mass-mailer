'''
Created on Apr 9, 2013

@author: CCMT
'''
import datetime
import time
import hashlib

from django.contrib.auth.models import User
from django.db import models


class Member(models.Model):
    
    GENDER_CHOICES = (('MALE', 'Male'),
                      ('FEMALE', 'Female'))
    
    user = models.ForeignKey(User)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.EmailField(max_length=150)
    gender = models.CharField(max_length=6, choices=GENDER_CHOICES)
    dob = models.DateField()
    phone = models.CharField(max_length=20)
    organization = models.CharField(max_length=50)

    def __unicode__(self):
        return "%s %s" %(self.first_name, self.last_name)    
    
def doc_upload_path(instance, filename):
    """
    The uploaded contacts file will be stored inside
    MEDIA_ROOT/contacts/<username>/<group_name> directory
    """
    return "contacts/%s/%s" % (instance.user.username, filename.replace(' ','-'))

class Group(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length=30)
    description = models.CharField(max_length=200)
    short_code = models.CharField(max_length=30)
    contacts_filepath = models.FileField(upload_to=doc_upload_path)
    member = models.ManyToManyField(Member)
    
    def __unicode__(self):
        return self.name
    
def generate_short_code(user):
    """Function to generate short code for group
    """
    t = datetime.datetime.now()
    uts = int(time.mktime(t.timetuple()))

    if user.email not in ['', None]:
        salt = '%s%s' % (user.email, uts)
    else:
        salt = '%s%s' % (user.username, uts)

    return hashlib.sha1(salt).hexdigest()