'''
Created on Apr 9, 2013

@author: CCMT
'''
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.template.loader import render_to_string #, Context
from django.http import HttpResponse
from django.utils import simplejson
from django.http import Http404
#from django.views.decorators.csrf import csrf_exempt

from Group.forms import GroupForm, ContactsUploadForm
from Group.models import Group


@login_required(login_url='/auth/')
def group(request):
    groups = Group.objects.all()
    return render_to_response('group/all_groups.html',
                              {'groups':groups},
                              context_instance=RequestContext(request))


@login_required(login_url='/auth/')
def create_group(request):
    if not request.is_ajax():
        raise Http404

    if request.method == 'POST':
        form = GroupForm(data=request.POST)
        if form.is_valid():
            form.save(request.user)
            groups = Group.objects.all()
            html = render_to_string('group/groups.html', 
                                    {'groups':groups},
                                    context_instance=RequestContext(request))
        else:
            html = render_to_string('group/group_form.html',
                                    {'form':form},
                                    context_instance=RequestContext(request))

    else:
        form = GroupForm()
        html = render_to_string('group/group_form.html',
                                {'form':form},
                                context_instance=RequestContext(request))

    data = {'html': html}
    json = simplejson.dumps(data)
    return HttpResponse(json,mimetype='application/json')
    

def contacts_upload(request):
    # Handle file upload
    if request.method == 'POST':
        form = ContactsUploadForm(request.POST, request.FILES)
        # if form is valid save the file to media directory and then redirect user to 
        # view all contacts. for now we are redirecting to groups page. This is to be
        # changed later
        gid = int(request.POST.get('gid'))
        if form.is_valid():
            group = Group.objects.get(pk=gid)
            group.contacts_filepath = request.FILES['contacts_filepath']
            group.save()
            groups = Group.objects.all()
            html = render_to_string('group/groups.html', 
                                    {'groups':groups},
                                    context_instance=RequestContext(request))
        else:
            html = render_to_string('group/contacts_upload_form.html',
                                {'form':form, 'gid':gid},
                                context_instance=RequestContext(request))
    else:
        gid = request.GET.get('gid')
        form = ContactsUploadForm(initial={'gid':gid})
        html = render_to_string('group/contacts_upload_form.html',
                                {'form':form, 'gid':gid},
                                context_instance=RequestContext(request))

    data = {'html': html}
    json = simplejson.dumps(data)
    return HttpResponse(json,mimetype='application/json')