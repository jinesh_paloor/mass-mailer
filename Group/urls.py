'''
Created on Apr 9, 2013

@author: CCMT
'''
from django.conf.urls import patterns, url

urlpatterns = patterns('Group.views',
                       url(r'^$', 'group', name="group"),
                       url(r'^create/$', 'create_group', name="create-group"),
                       url(r'^contacts/upload/$', 'contacts_upload', name="contacts-upload"),
                       )
